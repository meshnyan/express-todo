import 'reflect-metadata';
import express from "express";
import bodyParser from 'body-parser';
import cors from 'cors'
import {CronJob} from 'cron'

import * as Utils from './helpers/utils';
import { Singleton } from './helpers/singleton'
import {dbConnection} from './db/connection';
import {TokenStatus} from './entities/token'
import authRouter from './middlewares/authRouter'
import todoRouter from './middlewares/todoRouter'

dbConnection.then(
    connection => {
        Singleton.getInstance(connection)

/// Переменные
        const todoApp = express();
        const PORT = process.env.PORT || 3000;

///cron job
        const signInTimeout = new CronJob('0 0 */1 * * *', async () => {
            console.log('sign in timeout')
            const date = new Date()
            const tokens = await Utils.findAllActiveTokens(Singleton.getInstance().tokenRepo);
            tokens.forEach(value => {
                if((date.valueOf() - value.lastActivity.valueOf()) > 1000*60*60) {
                    Utils.dbSave(Singleton.getInstance().tokenRepo, {
                        id: value.id,
                        status: TokenStatus.DELETED
                    })
                }
            })
        })

        signInTimeout.start();

///Логика
        todoApp.disable('x-powered-by');

        todoApp.use(bodyParser.urlencoded({ extended: false}));
        todoApp.use(bodyParser.json(), (err, req, res, next) => {
            if (err) {
                res.status(400).send({
                    message: 'Invalid JSON',
                    status: 400
                })
            } else {
                next();
            }
        });
        todoApp.use(cors());
        
        todoApp.use('/', authRouter)
        todoApp.use('/todo', todoRouter)        

        todoApp.use((err, req, res, next) => {
            console.error(err.stack);
            res.status(500).send('Asgard has been defeated')
        })

        todoApp.listen(PORT, () => {
            console.log(`\nServer is running in http://localhost:${PORT}\n`)
        })
    }
)