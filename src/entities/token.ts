import { 
    Entity, Column, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, Generated
} from "typeorm";
import { User } from './user'

export enum TokenStatus {
    ACTIVE = 'active',
    DELETED = 'deleted',
}

@Entity()
export class Token {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column({
        type: 'enum',
        enum: TokenStatus,
        default: TokenStatus.ACTIVE
    })  
    status: TokenStatus;

    @CreateDateColumn()
    createdDate: Date;

    @Column({
        default: new Date()
    })
    lastActivity: Date;

    @ManyToOne(() => User, user => user.token)
    user: User;
}