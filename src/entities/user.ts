import { 
    Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, CreateDateColumn, UpdateDateColumn
} from "typeorm";
import { Todo } from './todo';
import { Token } from './token';

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        unique: true
    }) 
    login: string;

    @Column()
    password: string;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @OneToMany(type => Todo, t => t.user)
    todos: Todo[];

    @OneToMany(type => Token, t => t.user)
    token: Token;
}