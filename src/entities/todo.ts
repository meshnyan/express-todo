import { 
    Entity, Column, ManyToOne, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn
} from "typeorm";
import { User } from './user';

@Entity()
export class Todo {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column()  
    text: string;

    @Column({
        default: false
    })  
    completed: Boolean;

    @Column({
        default: false
    })  
    deleted: Boolean;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @ManyToOne(() => User, t => t.todos)
    user: User;
}