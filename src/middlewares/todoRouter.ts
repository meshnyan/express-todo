import express from "express";

import * as Utils from '../helpers/utils'
import {Singleton} from '../helpers/singleton'
import validate from "validate.js";
import checkTokenIsActive from '../middlewares/checkTokenIsActive'
import { asyncHandler } from "../helpers/async-handler";

const todoRouter = express.Router();

todoRouter.use(asyncHandler(checkTokenIsActive));

todoRouter.get('/', async (req, res) => {
    const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, req.headers.token as string);
    const todos = await Utils.findUsersTodos(Singleton.getInstance().todoRepo, user);
    res.send({
        message: 'ok',
        status: 200,
        todos
    })
})

todoRouter.post('/', async (req, res) => {
    const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, req.headers.token as string);
    const request = {
        text: req.body.text,
    }

    if(!validate(request, {
        text: {presence: true}
    })) {
        Utils.dbSave(Singleton.getInstance().todoRepo, {
            user,
            text: request.text
        });
    } else {
        res.send({
            message: 'Invalid todo, text field is empty',
            status: 400
        })
    }
    res.send({
        message: 'Todo was recorded successful',
        status: 200
    })
})

todoRouter.patch('/:id', async (req, res) => {
    const request = {
        id: req.params.id,
        text: req.body.text,
        completed: req.body.completed
    }

    if(
        (
            (validate(request, {
                text: {presence: true},
            }) !== undefined)
            &&
            (validate(request, {
                completed: {presence: true},
            }) !== undefined)
        )
        ||
        (validate(request, {
            completed: {type: "boolean", presence: true},
        }) !== undefined)
        ||
        (validate(request, {
            text: {type: "string", presence: true},
        }) !== undefined)
    ) {
        res.send({
            message: 'Bad request. Text and completed fields should not empty, expected text type - string, expected completed type - boolean',
            status: 400
        })
        return
    }

    if (!request.id) {
        res.send({
            message: 'Bad request. Request has no todo id', 
            status: 400
        })
    }

    const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, req.headers.token as string);
    const todo = await Utils.findUsersTodoById(Singleton.getInstance().todoRepo, user, request.id)
    if (todo) {
        if ((request.completed !== undefined) && (todo.completed !== request.completed)) {
            todo.completed = request.completed
        }

        if(request.text !== undefined) {
            todo.text = request.text
        }

        Utils.dbSave(Singleton.getInstance().todoRepo, {
            user,
            id: request.id,
            text: todo.text,
            completed: todo.completed
        })
        
        res.send({
            message: 'ok',
            status: 200
        })
    }
    else {
        res.send({
            message: 'Todo id incorrect or not found',
            status: 400
        })
    }            
})

todoRouter.delete('/:id', async(req, res) => {
    const todoId = req.params.id;

    if (!todoId) {
        res.send({
            message: 'Bad request. Request has no todo id', 
            status: 400
        })
    }

    const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, req.headers.token as string);
    const todo = await Utils.findUsersTodoById(Singleton.getInstance().todoRepo, user, todoId)

    if (todo) {
        Utils.dbSave(Singleton.getInstance().todoRepo, {
            user,
            id: todoId,
            deleted: true
        })
        
        res.send({
            message: 'ok',
            status: 200
        })

    } else {
        res.send({
            message: 'Todo id incorrect or not found',
            status: 400
        })
    }
})

export default todoRouter;