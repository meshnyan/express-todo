import express from "express";
import validate from "validate.js";
import bcrypt from 'bcrypt';

import * as Utils from '../helpers/utils'
import {Singleton} from '../helpers/singleton'
import { TokenStatus } from '../entities/token';
import checkTokenIsActive from '../middlewares/checkTokenIsActive'

const authRouter = express.Router();

authRouter.post('/signup', async (req, res) => {
    const request = {
        login: req.body.login,
        password: req.body.password,
    }

    const response = {
        token: null,
        message: 'sign up is ok',
        status: 200
    }

    if(!validate(request, {
        login: {type: "string", presence: true},
        password: {type: "string", presence: true}
    })) {
        const user = await Utils.findUser(Singleton.getInstance().userRepo, request.login)
        
        if (user) {
            response.message = 'Conflict. User is already exist.';
            response.status = 409;
        } else {
            const pwdToSave = bcrypt.hashSync(request.password, 10)

            const addedUser = await Utils.dbSave(Singleton.getInstance().userRepo, {
                login: request.login,
                password: pwdToSave,
            })

            const userToken = await Utils.dbSave(Singleton.getInstance().userRepo, {
                user: addedUser
            })

            response.token = userToken.id
        }

    } else {
        response.message = "Bad request. Check login or pwd fields";
        response.status = 400
    }

    res.send(response)
    
})

authRouter.post('/auth', async (req, res) => {
    const request = {
        login: req.body.login,
        password: req.body.password,
    }

    const response = {
        token: null,
        message: 'Auth is ok',
        status: 200
    }

    if(!validate(request, {
        login: {type: "string", presence: true},
        password: {type: "string", presence: true}
    })) {
        
        const user = await Utils.findUser(Singleton.getInstance().userRepo, request.login);
        if (user) {
            response.token = await bcrypt.compare(request.password, user.password).then( async (result) => {
                if (result) {
                    const newToken = await Utils.dbSave(Singleton.getInstance().tokenRepo, {user});
                    return newToken.id;
                }
            })
            if(!response.token) {
                response.message = 'Incorrect password',
                response.status = 404
            }
        } else {
            response.message = 'Such user does not exist',
            response.status = 404
        }
    } else {
        response.message = "Bad request. Check login or pwd fields";
        response.status = 400
    }

    res.send(response)
})

authRouter.patch('/logout', checkTokenIsActive, async (req, res) => {
    const token = await Utils.findToken(Singleton.getInstance().tokenRepo, req.headers.token as string);
    token.status = TokenStatus.DELETED;
    Singleton.getInstance().tokenRepo.save(token)
    res.send({
        message: `token ${token.id} deactivated successful`,
        status: 200
    })
})

export default authRouter;