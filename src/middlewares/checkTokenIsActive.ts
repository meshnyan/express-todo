import * as Utils from '../helpers/utils'
import { Singleton } from '../helpers/singleton'
import { Token } from '../entities/token'

const checkTokenIsActive = async (req, res, next) => {
    if (req.headers.token) {
        const token = await Utils.isTokenActive(Singleton.getInstance().tokenRepo, req.headers.token as string)
        if (token) {
            Utils.dbSave(Singleton.getInstance().tokenRepo, {
                id: (token as Token).id,
                lastActivity: new Date()
            })
            next();
        }
        else {
            res.send({
                message: 'Request has unactive or invalid token. Put it in the request`s headers correct token, sign up or sign in',
                status: 400
            })
        }
    }
    else {
        res.send({
            message: 'Request has no token. Put it in the request`s headers, sign up or sign in',
            status: 400
        })
    }
}

export default checkTokenIsActive