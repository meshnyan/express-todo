import { Repository } from "typeorm"
import { User } from "../entities/user"
import {TokenStatus, Token} from '../entities/token'
import validate from 'uuid-validate'
import { Todo } from "../entities/todo"

export const dbSave = async (repo: Repository <any>, data: object) => {
    const a = repo.create(data);
    return repo.save(a);

}

export const findUser = async (repo : Repository <User>, login : string): Promise<User> => {
    return repo.findOne({
        where: {
          login
        }
    })
}

export const findToken = async (repo : Repository <Token>, token : string): Promise<Token> => {
    return repo.findOne({
        where: {
          id: token,
          status: TokenStatus.ACTIVE
        }
    })
}

export const findAllActiveTokens = async (repo : Repository <Token>): Promise<Token[]> => {
    return repo.find({
        where: {
          status: TokenStatus.ACTIVE
        }
    })
}

export const isTokenActive = async (repo: Repository <Token>, sessionToken: string): Promise<Boolean|Token> => {

    if (validate(sessionToken, 4)) {
        const dbToken = await findToken(repo, sessionToken)
        if (dbToken) {
            return dbToken
        } else {
            return false
        }
    } else {
        return false
    }
}

export const findUserByToken = async (repo: Repository<Token>, token: string) => {
    const tokenInfo = await repo.findOne({
        relations: ["user"],
        where: {
            id: token
        }
    });
    return tokenInfo.user
}

export const findUsersTodos = async (repo: Repository<Todo>, user: User) => {
    const todos = await repo.find({
        where: {
            user,
            deleted: false
        }
    })
    return todos
}

export const findUsersTodoById = async (repo: Repository<Todo>, user: User, id: string) => {
    if (validate(id, 4)) {
        const todos = await repo.findOne({
            where: {
                user,
                id
            }
        })
        return todos
    } else {
        return false
    }
}