import {Token} from '../entities/token'
import { Repository } from 'typeorm';
import { Todo } from '../entities/todo';
import { User } from '../entities/user';
import { getUserRepository } from '../db/user.repository'
import { getTodoRepository } from '../db/todo.repository'
import { getTokenRepository } from '../db/token.repository'

export namespace Singleton {
	
    interface Instance {
        userRepo: Repository <User>,
        todoRepo: Repository <Todo>,
        tokenRepo: Repository <Token>,
    }

    let instance :Instance;

    export function getInstance(connection = null) :Instance {
        if(connection)
            instance = {
                userRepo: getUserRepository(connection),
                todoRepo: getTodoRepository(connection),
                tokenRepo: getTokenRepository(connection),
            };
        return instance
    }
  }