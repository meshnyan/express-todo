import { Connection } from 'typeorm'
import { Todo } from '../entities/todo'

export const getTodoRepository = (connection: Connection) => connection.getRepository(Todo)