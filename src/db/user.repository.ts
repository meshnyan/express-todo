import { Connection } from 'typeorm'
import { User } from '../entities/user'

export const getUserRepository = (connection: Connection) => connection.getRepository(User)